//
// Created by Adam Wilson on 10/05/2018.
//

#pragma once

#include "c4.hh"
#include <iostream>



using namespace std;

// TODO this should return Document once class is populated

class DatabaseException : public exception
{
public:
    DatabaseException(C4Error err) : error (err) {}
    C4Error& what()                { return error; }
    C4ErrorDomain getDomain()      { return error.domain; }
    int32_t getCode()              { return error.code; }

    C4Error error;
};

namespace util
{

    // Converts a slice to a C++ string
    static inline std::string toString(C4Slice s)   {return std::string((char*)s.buf, s.size);}


//    // Converts JSON5 to JSON; helps make JSON input more readable!
//    std::string json5(std::string str) {
//        FLError err;
//        FLSliceResult json = FLJSON5_ToJSON({str.data(), str.size()}, &err);
//        std::string result((char*)json.buf, json.size);
//        FLSliceResult_Free(json);
//        return result;
//    }
//
//
//    fleece::alloc_slice json5slice(std::string str) {
//        FLError err;
//        FLSliceResult json = FLJSON5_ToJSON({str.data(), str.size()}, &err);
//        return json;
//    }


    C4Slice stdstringToSlice (const std::string& s)
    {
        return {s.c_str(), s.size()};
    }

    C4SliceResult stdstringToSliceResult (std::string& s)
    {
        return {(void *) s.c_str(), s.size()};
    }

    const std::string sliceToStdstring(C4Slice s) {
        if (!s.buf)
            return std::string();

        return std::string ((char *) s.buf, s.size);
    }

    const std::string sliceResultToStdstring(C4SliceResult s) {
        if (!s.buf)
            return std::string();

        return std::string ((char *) s.buf, s.size);
    }

    void printError(const C4Error& c4Error, const std::string message = std::string())
    {
        std::cerr << (message.empty() ? message : std::string(message + " "))
            << "code:" << c4Error.code << ", domain:" << c4Error.domain << std::endl;
    }
};



