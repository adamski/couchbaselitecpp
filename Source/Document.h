/*
  ==============================================================================

    Document.h
    Created: 28 May 2017 12:27:03am
    Author:  Adam Wilson

  ==============================================================================
*/

#pragma once

#include "c4.hh"
#include <string>
#include "Utilities.h"

using namespace std;
using namespace fleeceapi;
using namespace util;

class Document
{
public:
    Document (C4Database* c4db)
    {
        this->c4db = c4db;
    }

    Document (C4Database* c4db, C4Document* newC4Doc, bool withBody = true)
    {
        this->c4db = c4db;
        c4doc = newC4Doc;
        if (withBody)
            loadRevisionBody();

        hasInitialised = true;
    }

    ~Document()
    {
        if (hasInitialised && c4doc != nullptr)
            c4doc_free (c4doc);
    }

    Document (const Document &doc) {
        hasInitialised = true;
        c4doc = doc.getC4Doc();
    }


    void loadRevisionBody() const
    {
        C4Error err;
        if (! c4doc_loadRevisionBody (c4doc, &err))
            printError (err, "Error loading revision body");
    }

    C4Document* getC4Doc() const { return c4doc; };

    string getBodyAsJSON() const
    {
        C4Error err;

        loadRevisionBody();

        C4SliceResult body = c4doc_bodyAsJSON (c4doc, false, &err);

        if (body.buf != NULL)
        {
            string jsonString = sliceResultToStdstring (body);
            c4slice_free (body);
            return jsonString;
        }
        else
        {
            printError (err, "Error getting JSON body");
            return string();
        }
    }

    Value getFleeceValue()
    {
        return Value::fromData (c4doc->selectedRev.body);
    }

    Dict getFleeceDictionary()
    {
        return getFleeceValue().asDict();
    }

    Value get (const string key)
    {
        auto sk = c4db_getFLSharedKeys (c4db); // TODO: Hide need for sk, e.g. `doc.get (key)`
        return getFleeceDictionary().get (key.c_str(), sk);
    }

    string getAsString (const string key)
    {
        auto sk = c4db_getFLSharedKeys (c4db); // TODO: Hide need for sk, e.g. `doc.get (key)`
        return getFleeceDictionary().get (key.c_str(), sk).asstring();
    }

    string update (string bodyAsJSON)
    {
        c4::Transaction transaction (c4db);
        if (!transaction.begin (&error))
        {
            printError(error, "Error beginning transaction");
            return string();
        }

        C4SliceResult body (c4db_encodeJSON(c4db, c4str(bodyAsJSON.c_str()), &error));

        if (body.buf == nullptr)
        {
            printError (error, "Error encoding JSON");
            c4slice_free (body);
            transaction.abort (&error);
            return {};
        }

        C4Document* outDoc = c4doc_update (c4doc, (C4Slice) body, 0, &error);
        c4slice_free (body);

        if (outDoc == nullptr)
        {
            printError (error, "Error updating document");
            transaction.abort (&error);
            return {};
        }

        bool success = transaction.commit (&error);

        if (! success)
        {
            printError (error, "Error committing document");
            return {};
        }

        c4doc_free (c4doc);
        c4doc = outDoc;

        return sliceToStdstring (outDoc->revID);
    }

    const C4Slice& getDocID()       { return c4doc->docID; }
    const C4Slice& getRevID()       { return c4doc->revID; }
    bool isValid()                  { return c4doc != nullptr; }
    const C4Error& getLastError()   { return error; }

private:
    bool hasInitialised = false;
    C4Error error;
    C4Document* c4doc = nullptr;
    C4Database* c4db = nullptr;
};



