/*
  ==============================================================================

    Database.h
    Created: 28 May 2017 12:26:50am
    Author:  Adam Wilson

  ==============================================================================
*/

#pragma once

#include "c4.hh"
#include "c4Document+Fleece.h"
#include <string>

#include "Utilities.h"
#include "Document.h"

using namespace std;
using namespace util;

class Database
{
public:
    Database()
    {
        // Default config
        kDBConfig = {
                .flags = (kC4DB_Create | kC4DB_AutoCompact | kC4DB_SharedKeys),
                .storageEngine = kC4SQLiteStorageEngine,
                .versioning = kC4RevisionTrees,
        };
    }
    
    ~Database()
    {
        close();
    }

    bool open (const string& path)
    {
        C4Error err;

        const C4String& dbPathStr = stdstringToSlice (path);
        
        cout <<  dbPathStr.buf;

        c4db = c4db_open(dbPathStr,
                         &kDBConfig,
                         &err);

        return c4db != nullptr;

    }

    void close()
    {
        if (c4db != nullptr)
            c4db_free (c4db);
    }

    Document getDocument (string documentId, bool mustExist, bool withBody = true)
    {
        C4Error err;
        C4Document* doc = c4doc_get(c4db, stdstringToSlice (documentId), true, &err);

        if (doc == nullptr)
        {
            if (mustExist)
                throw DatabaseException (err);
            else
            {
                c4::Transaction t (c4db);
                if (! t.begin(&err))
                    printError (err, "Error beginning transaction (create new document)");

                doc = c4doc_create (c4db, stdstringToSlice (documentId), {}, 0, &err);
                if (!doc)
                {
                    printError (err, "Error creating new document:" + documentId);
                    t.abort (&err);
                }
                else
                {
                    if (! t.commit (&err))
                        printError (err, "Error committing new document:" + documentId);
                }


                return Document (c4db, doc, false); // No body as the doc is empty
            }
        }
        return Document (c4db, doc, withBody);
    }

    C4Database* getC4Database() { return c4db; }

private:
    C4Database* c4db;
    C4DatabaseConfig kDBConfig;
    C4String dbPath;

};
