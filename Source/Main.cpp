/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "Database.h"
#include "c4.hh"
#include <string>

#include "FleeceCpp.hh"
#include "Replicator/CivetWebSocket.hh"

#define CATCH_CONFIG_MAIN
#include "../vendor/catch/catch.hpp"

using namespace fleeceapi;
using namespace std;
using namespace fleece;

class Query
{
public:

    Query(Database& db)
    {
        c4db = db.getC4Database();
    }

    ~Query() {
        c4query_free(query);
    }

    void compileSelect(const string &queryStr) {
        cout << "Query = " << queryStr;
        C4Error error;
        c4query_free(query);
        query = c4query_new(c4db, c4str(queryStr.c_str()), &error);

        if (query == NULL)
            printError (error, "c4query");

        REQUIRE(query);
    }

    void compile(const string &whereExpr,
                 const string &sortExpr ="",
                 bool addOffsetLimit =false)
    {
        stringstream json;
        json << "[\"SELECT\", {\"WHAT\": [[\"._id\"]], \"WHERE\": " << whereExpr;
        if (!sortExpr.empty())
            json << ", \"ORDER_BY\": " << sortExpr;
        if (addOffsetLimit)
            json << ", \"OFFSET\": [\"$offset\"], \"LIMIT\":  [\"$limit\"]";
        json << "}]";
        compileSelect(json.str());
    }

    // Runs query, invoking callback for each row and collecting its return values into a vector
    template <class Collected>
    vector<Collected> runCollecting(const char *bindings,
                                    function<Collected(C4QueryEnumerator*)> callback)
    {
        REQUIRE(query);
        C4QueryOptions options = kC4DefaultQueryOptions;
        C4Error error;
        auto e = c4query_run(query, &options, c4str(bindings), &error);
        INFO("c4query_run got error " << error.domain << "/" << error.code);
        REQUIRE(e);
        vector<Collected> results;
        while (c4queryenum_next(e, &error))
            results.push_back(callback(e));
        CHECK(error.code == 0);
        c4queryenum_free(e);
        return results;
    }

    // TODO get run methods working (Error:Undefined symbol 'fleece::alloc_slice::alloc_slice(FLSliceResult&&)')

    // Runs query, returning vector of doc IDs
    vector<string> run(const char *bindings =nullptr) {
        return runCollecting<string>(bindings, [&](C4QueryEnumerator *e) {
            REQUIRE(FLArrayIterator_GetCount(&e->columns) > 0);
            fleece::alloc_slice docID = FLValue_ToString(FLArrayIterator_GetValueAt(&e->columns, 0));
            return docID.asString();
        });
    }

    // Runs query, returning vector of doc IDs
    vector<string> run2(const char *bindings =nullptr) {
        return runCollecting<string>(bindings, [&](C4QueryEnumerator *e) {
            REQUIRE(FLArrayIterator_GetCount(&e->columns) >= 2);
            auto c1 = FLValue_ToString(FLArrayIterator_GetValueAt(&e->columns, 0));
            auto c2 = FLValue_ToString(FLArrayIterator_GetValueAt(&e->columns, 1));
            return sliceResultToStdstring (c1) + ", " + sliceResultToStdstring (c2);
        });
    }

    // Runs query, returning vectors of FTS matches (one vector per row)
    vector<vector<C4FullTextMatch>> runFTS(const char *bindings =nullptr) {
        return runCollecting<vector<C4FullTextMatch>>(bindings, [&](C4QueryEnumerator *e) {
            return vector<C4FullTextMatch>(&e->fullTextMatches[0],
                                           &e->fullTextMatches[e->fullTextMatchCount]);
        });
    }

protected:
    C4Database *c4db;
    C4Query *query {nullptr};
};

enum ReplicationMode
{
    disabled,        // Do not allow this direction
    passive,         // Allow peer to initiate this direction
    oneShot,         // Replicate, then stop
    continuous       // Keep replication active until stopped by application
};

enum ReplicationStatus
{
    stopped,     ///< Finished, or got a fatal error.
    offline,     ///< Not used by LiteCore; for use by higher-level APIs. */
    connecting,  ///< Connection is in progress.
    idle,        ///< Continuous replicator has caught up and is waiting for changes.
    busy         ///< Connected and actively working.
};

class Replicator
{
public:

    Replicator (Database& db)
    {
        c4db = db.getC4Database();
    }

    void replicatorStatusChanged(C4ReplicatorStatus status)
    {
        if (status.error.code > 0)
        {
            c4repl_free (c4repl);
            cout << "replication error: " << status.error.code << endl;
            return;
        }

        switch (status.level)
        {
            case kC4Stopped:
            {
                c4repl_free (c4repl);
                cout << "replication stopped" << endl;
                break;
            }

            case kC4Offline:
            {
                cout << "replication offline" << endl;
                break;
            }

            case kC4Connecting:
            {
                cout << "replication connecting" << endl;
                break;
            }

            case kC4Idle:
            {
                cout << "replication connecting" << endl;
                break;
            }

            case kC4Busy:
            {
                cout << "replication busy" << endl;
                break;
            }

            default:
                cout << "replication unknown" << endl;
                break;
        }

    }

    bool startReplication (string url, string remoteDatabaseName, ReplicationMode pull, ReplicationMode push)
    {

        C4String c4repUrl = stdstringToSlice (url);
        C4String c4remoteDbName = stdstringToSlice (remoteDatabaseName);
        C4Address remoteAddress;

        if (!c4address_fromURL (c4repUrl, &remoteAddress, &c4remoteDbName))
        {
            cerr << "Unable to read remote URL" << endl;
            return false;
        }

        C4ReplicatorParameters params {};
        //params.pull = params.push = mode;
        params.pull = static_cast<C4ReplicatorMode> (pull);
        params.push = static_cast<C4ReplicatorMode> (push);

        cout << "Pull mode: " << params.pull << endl;
        cout << "Push mode: " << params.push << endl;

        params.callbackContext = (void*) this;
        params.onStatusChanged = [](C4Replicator*, C4ReplicatorStatus status, void *context) {
            ((Replicator*)context)->replicatorStatusChanged (status);
        };

        C4Error error;

        c4repl = c4repl_new (c4db, remoteAddress, c4remoteDbName, nullptr, params, &error);

        if (!c4repl)
        {
            printError (error, "Error starting replication");
            return false;
        }

        int count = 1000;
        while (--count > 0)
        {
            sleep (50);
        }

    }

    ~Replicator ()
    {
        c4repl_free (c4repl);
    }

private:
    C4Replicator* c4repl;
    C4Database* c4db;
};


/***
 * C++ Wrapper for CouchbaseLite - LiteCore
 * TODO: Add replication: https://github.com/couchbase/couchbase-lite-ios/blob/master/Objective-C/CBLReplicator.mm
 * TODO: Add blobs: https://github.com/couchbase/couchbase-lite-ios/blob/master/Objective-C/CBLBlob.mm
 * TODO: Add Query functionality too...
 *
 */

static const C4DatabaseConfig kDBConfig = {
    .flags = (kC4DB_Create | kC4DB_AutoCompact | kC4DB_SharedKeys),
    .storageEngine = kC4SQLiteStorageEngine,
    .versioning = kC4RevisionTrees,
};

//==============================================================================
// TODO move these to JUCE wrapper
///// JUCE string conversions
//C4Slice stringToSlice (const String &s) {
//    return {s.toRawUTF8(), static_cast<size_t>(s.length())};
//}
//
//const String sliceToString (C4Slice s) {
//    if (!s.buf)
//        return String();
//
//    return String::fromUTF8((char*)s.buf, (int) s.size);
//}

//==============================================================================

TEST_CASE( "valid json can be committed to database", "[database]" )
{
    Database db;
    std::string dbPath = "../../../../c4db/db"; // TODO: Put this file and the test db in a `Tests` folder
    bool success = db.open (dbPath);

    REQUIRE (success == true);

    SECTION ("can create or update a document") // TODO: split into create & update, delete db file on startup: c4db_delete
    {
        Document doc = db.getDocument ("test", false);
        std::string bodyAsJSON ("{\"yes\":\"it's working\"}");
        string revID = doc.update (bodyAsJSON);

        REQUIRE (! revID.empty());
        REQUIRE (doc.getBodyAsJSON() == bodyAsJSON);

        string result = doc.get ("yes").asstring();
        REQUIRE (result == "it's working");
    }
}

TEST_CASE ( "replicate from a remote CouchDB database", "[database]")
{
    Database db;
    std::string dbPath = "../../../../c4db/db"; // TODO: Put this file and the test db in a `Tests` folder
    bool success = db.open (dbPath);

    REQUIRE (success == true);

    static std::once_flag once;
    std::call_once(once, [&] {
        c4socket_registerFactory(C4CivetWebSocketFactory);
    });

    SECTION ("replicate to local db")
    {
        Replicator replicator (db);
        replicator.startReplication ("http://159.65.208.142:5984/intatune", "intatune", ReplicationMode::oneShot, ReplicationMode::passive);
    }
}

TEST_CASE ( "query can be made on existing data", "[database]")
{
    Database db;
    std::string dbPath = "../../../../c4db/db"; // TODO: Put this file and the test db in a `Tests` folder
    bool success = db.open (dbPath);

    REQUIRE (success == true);

    SECTION ("can query existing documents")
    {
        C4Error error;

        Query query (db);
        query.compile ("[\"=\", [\".\", \"yes\"], \"it\'s working\"]");

        query.run();

    }
}


//    string revID = doc.update ("{\"yes\":\"actually working\"}");
//    string revID = doc.update ("{\"no\":\"won't work\"");
//    string revID = doc.update ("{no:404}");
